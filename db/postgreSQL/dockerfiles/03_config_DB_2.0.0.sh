#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "btbservice" --dbname "$APP_DBNAME" <<-EOSQL


INSERT INTO REF_NOTIFICATION_STATUS ( "id", "name" ) VALUES ( 1, 'offen' );
INSERT INTO REF_NOTIFICATION_STATUS ( "id", "name" ) VALUES ( 2, 'in Arbeit' );
INSERT INTO REF_NOTIFICATION_STATUS ( "id", "name" ) VALUES ( 3, 'erledigt' );
INSERT INTO REF_NOTIFICATION_STATUS ( "id", "name" ) VALUES ( 4, 'geschlossen' );

INSERT INTO REF_BRANCH ("id", "name", "description" ) VALUES ( 1, 'S', 'Strom' );
INSERT INTO REF_BRANCH ("id", "name", "description" ) VALUES ( 2, 'G', 'Gas' );
INSERT INTO REF_BRANCH ("id", "name", "description" ) VALUES ( 3, 'F', 'Fernwärme' );
INSERT INTO REF_BRANCH ("id", "name", "description" ) VALUES ( 4, 'W', 'Wasser' );
INSERT INTO REF_BRANCH ("id", "name", "description" ) VALUES ( 5, 'Z', 'ZSM' );

INSERT INTO REF_GRID_TERRITORY ("id", "name", "description", "fk_ref_master") VALUES ( 1, 'MA', 'Mannheim', 1);
INSERT INTO REF_GRID_TERRITORY ("id", "name", "description", "fk_ref_master") VALUES ( 2, 'OF', 'Offenbach', 2);
INSERT INTO REF_GRID_TERRITORY ("id", "name", "description", "fk_ref_master") VALUES ( 3, 'DR', 'Dreieich', 3);

DELETE FROM REF_VERSION;
INSERT INTO REF_VERSION VALUES (1, '1.0.0_PG');

INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (1, 1, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (1, 2, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (1, 3, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (1, 4, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (1, 5, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (2, 1, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (2, 2, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (2, 3, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (2, 4, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (2, 5, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (3, 1, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (3, 2, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (3, 3, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (3, 4, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (3, 5, 'admin','admin', CURRENT_TIMESTAMP);

INSERT INTO REF_NOTIFICATION_PRIORITY ("id", "name", "weighting", "image_name") VALUES ( 1, 'Wichtig', 30, 'prio-important-icon-web.svg');
INSERT INTO REF_NOTIFICATION_PRIORITY ("id", "name", "weighting", "image_name") VALUES ( 2, 'Information', 20, 'prio-info-icon-web.svg');
INSERT INTO REF_NOTIFICATION_PRIORITY ("id", "name", "weighting", "image_name") VALUES ( 3, 'Normal', 10, 'prio-normal-icon-web.svg');

EOSQL
