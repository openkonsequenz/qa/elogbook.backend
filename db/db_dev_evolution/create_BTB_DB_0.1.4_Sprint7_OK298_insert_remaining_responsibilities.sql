/* ********************************************************************************** */
 -- Task: [OK-298] Insert additional responsibilities (already missing in matrix). 
/* ********************************************************************************** */
 
INSERT INTO public."tbl_responsibility" (fk_ref_grid_territory, fk_ref_branch, responsible_user, create_user, create_date) VALUES (3, 2, 'admin','admin',now());
INSERT INTO public."tbl_responsibility" (fk_ref_grid_territory, fk_ref_branch, responsible_user, create_user, create_date) VALUES (3, 3, 'admin','admin',now());
INSERT INTO public."tbl_responsibility" (fk_ref_grid_territory, fk_ref_branch, responsible_user, create_user, create_date) VALUES (3, 4, 'admin','admin',now());
INSERT INTO public."tbl_responsibility" (fk_ref_grid_territory, fk_ref_branch, responsible_user, create_user, create_date) VALUES (4, 1, 'admin','admin',now());
INSERT INTO public."tbl_responsibility" (fk_ref_grid_territory, fk_ref_branch, responsible_user, create_user, create_date) VALUES (4, 3, 'otto','admin',now());
INSERT INTO public."tbl_responsibility" (fk_ref_grid_territory, fk_ref_branch, responsible_user, create_user, create_date) VALUES (4, 4, 'otto','admin',now());

-- Add an index to the responsibility_forwarding column of the notification table
-- The autocomplete function can be faster by searching the index instead of the String
CREATE INDEX responsibility_forwarding_idx
  ON public.tbl_notification(responsibility_forwarding);
