FROM tomcat:9.0-jdk8-adoptopenjdk-openj9
LABEL authors="dimitrios.chalepakis@pta.de, simon.reis@pta.de"

COPY deploy/conf/contextExternalConfig.xml /usr/local/tomcat/conf/context.xml

COPY deploy/conf/setenv.sh /usr/local/tomcat/bin/setenv.sh
RUN chmod +x /usr/local/tomcat/bin/setenv.sh

COPY elogbook.war /usr/local/tomcat/webapps/elogbook.war

COPY elogBookFE/ /usr/local/tomcat/webapps/elogBookFE/
