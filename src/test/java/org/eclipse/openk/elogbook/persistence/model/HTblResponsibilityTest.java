/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import org.junit.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class HTblResponsibilityTest {
    @Test
    public void testGettersAndSetters() {
        HTblResponsibility hTblResponsibility = new HTblResponsibility();

        hTblResponsibility.setId(1);
        assertEquals((long) hTblResponsibility.getId(), 1);

        hTblResponsibility.setTransactionId(1);
        assertEquals((long) hTblResponsibility.getTransactionId(), 1);

        LocalDateTime ldtCreate = LocalDateTime.parse("2017-06-19T15:00:00");
        Timestamp tsCreate = Timestamp.valueOf(ldtCreate);

        LocalDateTime ldtMod = LocalDateTime.parse("2017-06-20T15:00:00");
        Timestamp tsMod = Timestamp.valueOf(ldtMod);

        LocalDateTime ldtTransfer = LocalDateTime.parse("2017-07-20T15:00:00");
        Timestamp tsTransfer = Timestamp.valueOf(ldtTransfer);

        hTblResponsibility.setResponsibleUser("respoUser");
        assertEquals("respoUser", hTblResponsibility.getResponsibleUser());

        hTblResponsibility.setFormerResponsibleUser("formerRespoUser");
        assertEquals("formerRespoUser", hTblResponsibility.getFormerResponsibleUser());

        hTblResponsibility.setCreateDate(tsCreate);
        assertEquals(tsCreate, hTblResponsibility.getCreateDate());

        hTblResponsibility.setCreateUser("creatore");
        assertEquals("creatore", hTblResponsibility.getCreateUser());

        hTblResponsibility.setModDate(tsMod);
        assertEquals(tsMod, hTblResponsibility.getModDate());

        hTblResponsibility.setTransferDate(tsTransfer);
        assertEquals(tsTransfer, hTblResponsibility.getTransferDate());

        hTblResponsibility.setModUser("musr");
        assertEquals("musr", hTblResponsibility.getModUser());

        RefBranch rb = new RefBranch();
        rb.setDescription("brunch");
        hTblResponsibility.setRefBranch(rb);
        assertEquals("brunch", hTblResponsibility.getRefBranch().getDescription());

        RefGridTerritory refGridTerritory = new RefGridTerritory();
        refGridTerritory.setDescription("Mannheim");
        hTblResponsibility.setRefGridTerritory(refGridTerritory);
        assertEquals(hTblResponsibility.getRefGridTerritory().getDescription(),"Mannheim");

    }

}
