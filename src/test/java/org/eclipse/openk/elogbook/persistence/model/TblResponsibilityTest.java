/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import org.junit.Test;

public class TblResponsibilityTest {
    @Test
    public void TestGettersAndSetters() {
        TblResponsibility tblResponsibility = new TblResponsibility();

        tblResponsibility.setId(1);
        assertEquals((long) tblResponsibility.getId(), 1);

        LocalDateTime ldtCreate = LocalDateTime.parse("2017-06-19T15:00:00");
        Timestamp tsCreate = Timestamp.valueOf(ldtCreate);

        LocalDateTime ldtMod = LocalDateTime.parse("2017-06-20T15:00:00");
        Timestamp tsMod = Timestamp.valueOf(ldtMod);

        tblResponsibility.setResponsibleUser("respoUser");
        assertEquals("respoUser", tblResponsibility.getResponsibleUser());

        tblResponsibility.setNewResponsibleUser("newRespoUser");
        assertEquals("newRespoUser", tblResponsibility.getNewResponsibleUser());

        tblResponsibility.setCreateDate(tsCreate);
        assertEquals(tsCreate, tblResponsibility.getCreateDate());

        tblResponsibility.setCreateUser("creatore");
        assertEquals("creatore", tblResponsibility.getCreateUser());

        tblResponsibility.setModDate(tsMod);
        assertEquals(tsMod, tblResponsibility.getModDate());

        tblResponsibility.setModUser("musr");
        assertEquals("musr", tblResponsibility.getModUser());

        RefBranch rb = new RefBranch();
        rb.setDescription("brunch");
        tblResponsibility.setRefBranch(rb);
        assertEquals("brunch", tblResponsibility.getRefBranch().getDescription());

        RefGridTerritory refGridTerritory = new RefGridTerritory();
        refGridTerritory.setDescription("Mannheim");
        tblResponsibility.setRefGridTerritory(refGridTerritory);
        assertEquals(tblResponsibility.getRefGridTerritory().getDescription(),"Mannheim");

    }

}
