/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common;

import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class InitBackendConfigTest {

    private InitBackendConfig initBackendConfig = new InitBackendConfig();

    @Test
    public void testSetConfigFiles() throws Exception {
        Whitebox.invokeMethod(initBackendConfig, "setConfigFiles", "DevLocal");
    }

    private void testGeneric(String environment ) throws Exception {
        Whitebox.invokeMethod(initBackendConfig, "setConfigFiles", environment);
    }

    @Test
    public void testCombinations() throws Exception {

        testGeneric("DevLocal");
        testGeneric("DevLocal");

        testGeneric("DevServer");
        testGeneric("DevServer");

        testGeneric("Custom");
        testGeneric("Custom");

        testGeneric(null);
        testGeneric(null);

        testGeneric("");
        testGeneric("");

}
}
