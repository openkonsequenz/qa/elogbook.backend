/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common.mapper;


import org.eclipse.openk.elogbook.controller.NotificationTestHelper;
import org.eclipse.openk.elogbook.persistence.model.RefNotificationStatus;
import org.eclipse.openk.elogbook.persistence.model.TblNotification;
import org.eclipse.openk.elogbook.viewmodel.Notification;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;

public class NotificationMapperTest {

    public Notification createNotification(){

        Timestamp ts = new Timestamp(System.currentTimeMillis());
        Notification vmNot = new Notification();
        vmNot.setId(1);
        vmNot.setIncidentId(null);
        vmNot.setVersion(3);
        vmNot.setCreateDate(ts);
        vmNot.setCreateUser("Creator");
        vmNot.setExpectedFinishDate(ts);
        vmNot.setBeginDate(ts);
        vmNot.setFinishedDate(ts);
        vmNot.setFreeText("Free Willi");
        vmNot.setFreeTextExtended("...is still alive");
        vmNot.setModDate(ts);
        vmNot.setModUser("modificatore");
        vmNot.setNotificationText("notnot");
        vmNot.setReminderDate(ts);
        vmNot.setResponsibilityControlPoint("Resp1");
        vmNot.setResponsibilityForwarding("fwd");
        vmNot.setFkRefBranch(1);
        vmNot.setFkRefNotificationStatus(2);
        vmNot.setFkRefGridTerritory(1);
        vmNot.getAdminFlag(Boolean.TRUE);
        vmNot.setResponsibilityForwardingUuid(UUID.randomUUID());
        return vmNot;

    }

    @Test
    public void testMap() {

        Notification vmNot = createNotification();

        NotificationMapper mappy = NotificationTestHelper.createMapper();
        TblNotification tnot = mappy.mapFromVModel(vmNot);
        assertEquals(vmNot.getId(), tnot.getId());
        assertEquals(vmNot.getIncidentId(), tnot.getIncidentId());
        assertEquals(vmNot.getVersion(), tnot.getVersion());
        assertEquals(vmNot.getCreateDate().getTime(), tnot.getCreateDate().getTime());
        assertEquals(vmNot.getCreateUser(), tnot.getCreateUser());
        assertEquals(vmNot.getExpectedFinishDate().getTime(), tnot.getExpectedFinishedDate().getTime());
        assertEquals(vmNot.getFinishedDate().getTime(), tnot.getFinishedDate().getTime());
        assertEquals(vmNot.getBeginDate().getTime(), tnot.getBeginDate().getTime());
        assertEquals(vmNot.getFreeText(), tnot.getFreeText());
        assertEquals(vmNot.getFreeTextExtended(), tnot.getFreeTextExtended());
        assertEquals(vmNot.getModDate().getTime(), tnot.getModDate().getTime());
        assertEquals(vmNot.getModUser(), tnot.getModUser());
        assertEquals(vmNot.getNotificationText(), tnot.getNotificationText());
        assertEquals(vmNot.getReminderDate().getTime(), tnot.getReminderDate().getTime());
        assertEquals(vmNot.getResponsibilityControlPoint(), tnot.getResponsibilityControlPoint());
        assertEquals(vmNot.getResponsibilityForwarding(), tnot.getResponsibilityForwarding());
        assertEquals(vmNot.getFkRefBranch(), tnot.getRefBranch().getId());
        assertEquals(vmNot.getFkRefNotificationStatus(), tnot.getRefNotificationStatus().getId());
        assertEquals(vmNot.getFkRefGridTerritory(),  tnot.getRefGridTerritory().getId());
        assertEquals(vmNot.isAdminFlag(),  tnot.isAdminFlag());

        List<TblNotification> tblNotList = new ArrayList<>();
        tblNotList.add(tnot);
        List<Notification> notList = mappy.mapListToVModel(tblNotList);
        Notification rev = notList.get(0);
        assertNotNull(rev);
        assertEquals(vmNot.getId(), rev.getId());
        assertEquals(vmNot.getIncidentId(), rev.getIncidentId());
        assertEquals(vmNot.getVersion(), rev.getVersion());
        assertEquals(vmNot.getCreateDate().getTime(), rev.getCreateDate().getTime());
        assertEquals(vmNot.getCreateUser(), rev.getCreateUser());
        assertEquals(vmNot.getExpectedFinishDate().getTime(), rev.getExpectedFinishDate().getTime());
        assertEquals(vmNot.getBeginDate().getTime(), rev.getBeginDate().getTime());
        assertEquals(vmNot.getFinishedDate().getTime(), rev.getFinishedDate().getTime());
        assertEquals(vmNot.getFreeText(), rev.getFreeText());
        assertEquals(vmNot.getFreeTextExtended(), rev.getFreeTextExtended());
        assertEquals(vmNot.getModDate().getTime(), rev.getModDate().getTime());
        assertEquals(vmNot.getModUser(), rev.getModUser());
        assertEquals(vmNot.getNotificationText(), rev.getNotificationText());
        assertEquals(vmNot.getReminderDate().getTime(), rev.getReminderDate().getTime());
        assertEquals(vmNot.getResponsibilityControlPoint(), rev.getResponsibilityControlPoint());
        assertEquals(vmNot.getResponsibilityForwarding(), rev.getResponsibilityForwarding());
        assertEquals(vmNot.getFkRefBranch(), rev.getFkRefBranch());
        assertEquals(vmNot.getFkRefNotificationStatus(), rev.getFkRefNotificationStatus());
        assertEquals(vmNot.getFkRefGridTerritory(), rev.getFkRefGridTerritory());
        assertEquals(vmNot.isAdminFlag(), rev.isAdminFlag());
    }

    @Test
    public void testTblNotificationNull() {

        List<TblNotification> tblNotList = new ArrayList<>();
        NotificationMapper mappy = NotificationTestHelper.createMapper();
        tblNotList.add(null);
        List<Notification> notList = mappy.mapListToVModel(tblNotList);
        assertNull(notList.get(0));

    }

    @Test
    public void testTblNotificationModDateNull() {

        Notification vmNot = createNotification();
        vmNot.setModDate(null);
        NotificationMapper mappy = NotificationTestHelper.createMapper();
        TblNotification tnot = mappy.mapFromVModel(vmNot);
        assertNull(tnot.getModDate());

    }
    
    @Test
    public void testTblNotificationFkNull() {

       TblNotification tblNotification =  new TblNotification();
       tblNotification.setRefNotificationStatus(new RefNotificationStatus());
       NotificationMapper mappy = NotificationTestHelper.createMapper();
       Notification notification = mappy.mapToVModel(tblNotification);
       assertNull(notification.getFkRefBranch());
       assertNull(notification.getFkRefGridTerritory());
    }


    @Test
    public void testTblNotificationUuidNull() {

        TblNotification tblNotification =  new TblNotification();
        tblNotification.setRefNotificationStatus(new RefNotificationStatus());
        tblNotification.setResponsibilityForwardingUuid(null);
        NotificationMapper mappy = NotificationTestHelper.createMapper();
        Notification notification = mappy.mapToVModel(tblNotification);
        assertNull(notification.getFkRefBranch());
        assertNull(notification.getFkRefGridTerritory());
        assertNull(notification.getResponsibilityForwardingUuid());
        TblNotification tmodel = mappy.mapFromVModel(notification);
        assertNull(tmodel.getResponsibilityForwardingUuid());
    }
}
