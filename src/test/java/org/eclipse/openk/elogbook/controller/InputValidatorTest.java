/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;

import org.eclipse.openk.elogbook.common.Globals;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.eclipse.openk.elogbook.exceptions.BtbBadRequest;
import org.eclipse.openk.elogbook.exceptions.BtbUnauthorized;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class InputValidatorTest extends ResourceLoaderBase {

    // Check Credentials -------------------------------------------------------

    @Test(expected = BtbUnauthorized.class)
    public void testCheckCredentials_Null() throws BtbUnauthorized {
        new InputDataValuator().checkCredentials(null);
    }

    @Test(expected = BtbUnauthorized.class)
    public void testCheckCredentials_Empty() throws BtbUnauthorized {
        new InputDataValuator().checkCredentials("");
    }

    @Test(expected = BtbUnauthorized.class)
    public void testCheckCredentials_TooLong() throws BtbUnauthorized {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < Globals.MAX_CREDENTIALS_LENGTH; i++) {
            sb.append("W");
        }
        new InputDataValuator().checkCredentials(sb.toString());

    }

    @Test(expected = BtbUnauthorized.class)
    public void testCheckCredentials_Invalid() throws BtbUnauthorized {
        new InputDataValuator().checkCredentials("{\"abc\":\"123\"}");
    }

    @Test(expected = BtbUnauthorized.class)
    public void testCheckCredentials_InvalidNoJson() throws BtbUnauthorized {
        new InputDataValuator().checkCredentials("123");
    }

    @Test(expected = BtbUnauthorized.class)
    public void testCheckCredentials_tooLong() throws BtbUnauthorized {
        StringBuilder sb = new StringBuilder(Globals.MAX_CREDENTIALS_LENGTH + 1);
        for (int i = 0; i < Globals.MAX_CREDENTIALS_LENGTH + 1; i++) {
            sb.append("@");
        }
        new InputDataValuator().checkCredentials(sb.toString());

    }

    // -- check notifications

    @Test(expected = BtbBadRequest.class)
    public void testCheckNotification_null() throws BtbBadRequest {
        new InputDataValuator().checkNotification(null);
    }

    @Test(expected = BtbBadRequest.class)
    public void testCheckNotification_Empty() throws BtbBadRequest {
        new InputDataValuator().checkNotification("");
    }

    @Test(expected = BtbBadRequest.class)
    public void testCheckNotification_TooLong() throws BtbBadRequest {
        StringBuilder sb = new StringBuilder(Globals.MAX_NOTIFICATION_LENGTH + 1);
        for (int i = 0; i < Globals.MAX_NOTIFICATION_LENGTH + 1; i++) {
            sb.append("@");
        }
        new InputDataValuator().checkNotification(sb.toString());
    }

    // -- check notifications

    @Test
    public void testCheckNotificationSearchFilter_null() throws BtbBadRequest {
        new InputDataValuator().checkIncomingSearchFilter(null);
    }

    @Test
    public void testCheckNotificationSearchFilter_Empty() throws BtbBadRequest {
        new InputDataValuator().checkIncomingSearchFilter("");
    }

    @Test(expected = BtbBadRequest.class)
    public void testCheckNotificationSearchFilter_TooLong() throws BtbBadRequest {
        StringBuilder sb = new StringBuilder(Globals.MAX_NOTIFICATION_LENGTH + 1);
        for (int i = 0; i < Globals.MAX_NOTIFICATION_LENGTH + 1; i++) {
            sb.append("@");
        }
        new InputDataValuator().checkIncomingSearchFilter(sb.toString());
    }

    @Test
    public void testCheckNotification_OK() throws BtbBadRequest {
        new InputDataValuator().checkNotification("{}"); // at this time...empty is allowed ;-)
    }

    @Test
    public void checkWhitelistChars_ok() throws Exception {
        callWhitelistCheck("AaBbCcäÄöÖüÜß ?0123456789(). _+  ,-:;=!§%&/'#<>\"");
    }

    @Test(expected = BtbBadRequest.class)
    public void checkWhitelistChars_nok() throws Exception {
        callWhitelistCheck(null);
        callWhitelistCheck("");
        callWhitelistCheck("AaBbCc.,{}");
    }

    @Test(expected = BtbBadRequest.class)
    public void checkNotificationId_null() throws Exception {
        InputDataValuator.checkNotificationId(null);
    }

    @Test(expected = BtbBadRequest.class)
    public void checkNotificationId_empty() throws Exception {
        InputDataValuator.checkNotificationId("");
    }

    @Test(expected = BtbBadRequest.class)
    public void checkNotificationId_bad() throws Exception {
        InputDataValuator.checkNotificationId("aaa");
    }

    @Test
    public void checkNotificationId_ok() throws Exception {
        InputDataValuator.checkNotificationId("1234");
    }

    @Test(expected = BtbBadRequest.class)
    public void checkHistoricalResponsibilityId_null() throws Exception {
        InputDataValuator.checkHistoricalResponsibilityId(null);
    }

    @Test(expected = BtbBadRequest.class)
    public void checkHistoricalResponsibilityId_empty() throws Exception {
        InputDataValuator.checkHistoricalResponsibilityId("");
    }

    @Test(expected = BtbBadRequest.class)
    public void checkHistoricalResponsibilityId_bad() throws Exception {
        InputDataValuator.checkHistoricalResponsibilityId("aaa");
    }

    @Test
    public void checkHistoricalResponsibilityId_ok() throws Exception {
        InputDataValuator.checkHistoricalResponsibilityId("1234");
    }


    private void callWhitelistCheck(String tst) throws Exception {
        InputDataValuator idv = new InputDataValuator();
        Whitebox.invokeMethod(idv, "checkWhitelistChars", tst, false);
    }


}
