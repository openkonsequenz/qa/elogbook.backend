/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;


import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;

import java.util.LinkedList;
import java.util.List;
import org.eclipse.openk.elogbook.common.mapper.NotificationMapper;
import org.eclipse.openk.elogbook.persistence.dao.RefBranchDao;
import org.eclipse.openk.elogbook.persistence.dao.RefGridTerritoryDao;
import org.eclipse.openk.elogbook.persistence.dao.RefNotificationPriorityDao;
import org.eclipse.openk.elogbook.persistence.dao.RefNotificationStatusDao;
import org.eclipse.openk.elogbook.persistence.model.RefBranch;
import org.eclipse.openk.elogbook.persistence.model.RefGridTerritory;
import org.eclipse.openk.elogbook.persistence.model.RefNotificationPriority;
import org.eclipse.openk.elogbook.persistence.model.RefNotificationStatus;
import org.powermock.api.easymock.PowerMock;

public class NotificationTestHelper {
    private static RefNotificationStatus newStatusItem(int id, String name) {
        RefNotificationStatus ret = new RefNotificationStatus();
        ret.setId(id);
        ret.setName(name);
        return ret;
    }

    private static RefNotificationPriority newPriorityItem(int id, String name, int weighting) {
        RefNotificationPriority ret = new RefNotificationPriority();
        ret.setId(id);
        ret.setName(name);
        ret.setWeighting(weighting);
        return ret;
    }

    private static RefBranch newBranch(int id, String name, String description) {
        RefBranch ret = new RefBranch();
        ret.setId(id);
        ret.setName(name);
        ret.setDescription(description);
        return ret;
    }

    private static RefGridTerritory newRefGridTerritory(int id, String description, String name, int refMasterId) {
        RefGridTerritory refGridTerritory  = new RefGridTerritory();
        refGridTerritory.setId(id);
        refGridTerritory.setName(name);
        refGridTerritory.setDescription(description);
        refGridTerritory.setRefMaster(new RefGridTerritory());
        refGridTerritory.getRefMaster().setId(refMasterId);
        return refGridTerritory;
    }

    private static RefNotificationStatusDao createStatusDaoMock(List<RefNotificationStatus> rnsList) {
        RefNotificationStatusDao rnsdaoMock = PowerMock.createNiceMock(RefNotificationStatusDao.class);
        expect(rnsdaoMock.findInTx(true, -1, 0)).andReturn(rnsList);
        replay(rnsdaoMock);
        return rnsdaoMock;
    }

    private static RefNotificationPriorityDao createPriorityDaoMock(List<RefNotificationPriority> rnpList) {
        RefNotificationPriorityDao rnsdaoMock = PowerMock.createNiceMock(RefNotificationPriorityDao.class);
        expect(rnsdaoMock.findInTx(true, -1, 0)).andReturn(rnpList);
        replay(rnsdaoMock);
        return rnsdaoMock;
    }

    private static RefBranchDao createBranchDaoMock(List<RefBranch> rbList) {
        RefBranchDao rbdaoMock = PowerMock.createNiceMock(RefBranchDao.class);
        expect(rbdaoMock.findInTx(true, -1, 0)).andReturn(rbList);
        replay(rbdaoMock);
        return rbdaoMock;
    }

    private static RefGridTerritoryDao createRefGridTerritoryDaoMock(List<RefGridTerritory> refGridTerritories) {
        RefGridTerritoryDao refGridTerritoryDaoMock = PowerMock.createNiceMock(RefGridTerritoryDao.class);
        expect(refGridTerritoryDaoMock.findInTx(true, -1, 0)).andReturn(refGridTerritories);
        replay(refGridTerritoryDaoMock);
        return refGridTerritoryDaoMock;
    }

    public static NotificationMapper createMapper() {
        List<RefNotificationStatus> rnsList = new LinkedList<>();
        rnsList.add(newStatusItem(1, "offen"));
        rnsList.add(newStatusItem(2, "geschlossen"));
        RefNotificationStatusDao rnsDao = createStatusDaoMock(rnsList);

        List<RefBranch> rbList = new LinkedList<>();
        rbList.add(newBranch(1, "W", "Wasser"));
        rbList.add(newBranch(2, "G", "Gas"));
        RefBranchDao rbDao = createBranchDaoMock(rbList);

        List<RefGridTerritory> refGridTerritories = new LinkedList<RefGridTerritory>();
        refGridTerritories.add(newRefGridTerritory(1, "MA", "Mannheim", 1));
        refGridTerritories.add(newRefGridTerritory(2, "OF", "Offenbach", 1));
        RefGridTerritoryDao refGridTerritoryDao = createRefGridTerritoryDaoMock(refGridTerritories);

        List<RefNotificationPriority> priorityList = new LinkedList<>();
        priorityList.add(newPriorityItem(1,"Wichtig", 1));
        priorityList.add(newPriorityItem(2,"Information", 2));
        RefNotificationPriorityDao priorityDaoMock = createPriorityDaoMock(priorityList);

        return new NotificationMapper(rnsDao, rbDao, refGridTerritoryDao, priorityDaoMock);
    }
}
