/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.eclipse.openk.elogbook.common.BackendConfig;
import org.eclipse.openk.elogbook.exceptions.BtbInternalServerError;
import org.eclipse.openk.elogbook.viewmodel.NotificationFile;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class BackendControllerNotificationFileTest{

    private static int fileRowToRead = BackendConfig.getInstance().getFileRowToRead();
    BackendControllerNotificationFile backConF = new BackendControllerNotificationFile();

    @Test
    public void testGetNotificationFiles_Exception() throws BtbInternalServerError {
        backConF.getNotificationsFiles("getImportFiles");
    }

    @Test
    public void testGetNotificationsFiles() throws Exception {

        File[] listOfFiles = new File[1];
        File file = File.createTempFile( "testFile", ".txt");
        List<String> lines = Arrays.asList("line MA test", "line MA test", "line MA test", "line MA test", "line MA test", "line MA test", "line MA test", "line MA test", "line MA test", "line MA test");

        listOfFiles[0] = file;

        if (listOfFiles != null)
        {
            List<NotificationFile> notFiles = backConF.getNotificationsFiles("choice");
            Whitebox.invokeMethod(backConF, "processChoiceGetNotificationFilesWithChoice", "getImportFiles", notFiles, listOfFiles);

            Path filePath = Paths.get(listOfFiles[0].toString());
            Files.write(filePath, lines);
            List<String> allTheLines = Files.readAllLines(filePath, Charset.defaultCharset());
            if (allTheLines.size() >= fileRowToRead)
            {
                Whitebox.invokeMethod(backConF, "processChoiceGetNotificationFilesWithChoice", "importFile", notFiles, listOfFiles);

            }
        }
        file.deleteOnExit();
    }


    @Test
    public void testExtractFileData() throws Exception {

        NotificationFile nf = new NotificationFile();
        List<String> lines = new ArrayList<>();
        String line = "10:10:51,425 ME Text";
        String line2 = "10:10:51,425 OW Text";
        for (int i=0; i<5; i++)
        {
            lines.add(line);
        }
        for (int i=6; i<10; i++)
        {
            lines.add(line2);
        }
        lines.add("10:10:51,425 XX Text"); // unknown branch

        Whitebox.invokeMethod(backConF, "extractFileData", nf, lines);

        String[] splitedLine = line.split("\\s+", 3);

        Whitebox.invokeMethod(backConF, "setBranchAndTerritory", splitedLine[1], nf);

        nf.setNotificationText(splitedLine[2]);


    }


	@Test
	public void testDeleteNotificationsFile() {

		String fileName = "name";
		BackendControllerNotificationFile backendControllerNotificationFile = new BackendControllerNotificationFile();
		backendControllerNotificationFile.deleteImportedFile(fileName);

	}
}
