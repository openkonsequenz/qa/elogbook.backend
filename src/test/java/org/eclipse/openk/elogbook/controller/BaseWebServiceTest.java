/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;

import org.apache.http.HttpStatus;
import org.eclipse.openk.elogbook.auth2.model.JwtToken;
import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.eclipse.openk.elogbook.controller.BaseWebService.SecureType;
import org.eclipse.openk.elogbook.exceptions.BtbBadRequest;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.exceptions.BtbUnauthorized;
import org.junit.Assert;
import org.junit.Test;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.core.*;
import java.lang.annotation.Annotation;
import java.net.URI;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

public class BaseWebServiceTest extends ResourceLoaderBase {

	private static final Logger EMPTYLOGGER = 	LogManager.getLogger(BaseWebServiceTest.class.getName());

	String json = super.loadStringFromResource("JwtAdmin.json");
	JwtToken jwtToken = JsonGeneratorBase.getGson().fromJson(json, JwtToken.class);

	public static class TestWebService extends BaseWebService {
		public String sessionId;
		public String cookieId;
		public boolean throwUnauthException = false;

		public TestWebService() {
			super(EMPTYLOGGER);
		}

		@Override
		protected void assertAndRefreshToken(String token, SecureType secureType) throws BtbException {
			if (throwUnauthException) {
				throw new BtbUnauthorized();
			} else {
				this.sessionId = token;
			}
		}

	}

	public static class TestInvokable extends BackendInvokable {
		public boolean isInvoked = false;
		public BtbException exceptionToThrow = null;
		public boolean throwRuntime = false;
		public Response response = null;

		@Override
		public Response invoke() throws BtbException {
			if (exceptionToThrow != null) {
				throw exceptionToThrow;
			}
			if (throwRuntime) {
				((String) null).equals("error"); //NOSONAR
			}
			isInvoked = true;
			return response;
		}
	}
	
	@Test
	public void testInvokeException() {
		TestWebService tws = new TestWebService();
		TestInvokable ti = new TestInvokable();

		ti.exceptionToThrow = new BtbBadRequest();
		Response ret = tws.invoke(jwtToken.getAccessToken(), BaseWebService.SecureType.NORMAL, ti);
		Assert.assertEquals(HttpStatus.SC_BAD_REQUEST, ret.getStatus());
	}

	@Test
	public void testInvokeRuntimeException() {
		TestWebService tws = new TestWebService();
		TestInvokable ti = new TestInvokable();

		ti.throwRuntime = true;
		Response ret = tws.invoke(jwtToken.getAccessToken(), BaseWebService.SecureType.NORMAL, ti);
		assertEquals(HttpStatus.SC_INTERNAL_SERVER_ERROR, ret.getStatus());
	}

	@Test
	public void testUnauthException() {
		TestWebService tws = new TestWebService();
		TestInvokable ti = new TestInvokable();
		tws.throwUnauthException = true;
		Response ret = tws.invoke(jwtToken.getAccessToken(), BaseWebService.SecureType.NORMAL, ti);
		assertEquals(HttpStatus.SC_UNAUTHORIZED, ret.getStatus());
	}

	@Test
	public void testSecureTypeNone() {
		TestWebService tws = new TestWebService();
		TestInvokable ti = new TestInvokable();
		Response responseForTypeNONE = tws.invoke(jwtToken.getAccessToken(), SecureType.NONE, ti);
		Response responseForTypeHIGH = tws.invoke(jwtToken.getAccessToken(), SecureType.HIGH, ti);
		Response responseForTypeNORMAL = tws.invoke(jwtToken.getAccessToken(), SecureType.NORMAL, ti);
		assertNull(responseForTypeNONE);
		assertNull(responseForTypeHIGH);
		assertNull(responseForTypeNORMAL);
	}


	@Test
	public void testInvokation() {
		TestWebService tws = new TestWebService();
		TestInvokable ti = new TestInvokable();
		ti.response = new Response() {
			@Override
			public Object getEntity() {
				return null;
			}

			@Override
			public <T> T readEntity(Class<T> aClass) {
				return null;
			}

			@Override
			public <T> T readEntity(GenericType<T> genericType) {
				return null;
			}

			@Override
			public <T> T readEntity(Class<T> aClass, Annotation[] annotations) {
				return null;
			}

			@Override
			public <T> T readEntity(GenericType<T> genericType, Annotation[] annotations) {
				return null;
			}

			@Override
			public boolean hasEntity() {
				return false;
			}

			@Override
			public boolean bufferEntity() {
				return false;
			}

			@Override
			public void close() {

			}

			@Override
			public MediaType getMediaType() {
				return null;
			}

			@Override
			public Locale getLanguage() {
				return null;
			}

			@Override
			public int getLength() {
				return 0;
			}

			@Override
			public Set<String> getAllowedMethods() {
				return null;
			}

			@Override
			public Map<String, NewCookie> getCookies() {
				return null;
			}

			@Override
			public EntityTag getEntityTag() {
				return null;
			}

			@Override
			public Date getDate() {
				return null;
			}

			@Override
			public Date getLastModified() {
				return null;
			}

			@Override
			public URI getLocation() {
				return null;
			}

			@Override
			public Set<Link> getLinks() {
				return null;
			}

			@Override
			public boolean hasLink(String s) {
				return false;
			}

			@Override
			public Link getLink(String s) {
				return null;
			}

			@Override
			public Link.Builder getLinkBuilder(String s) {
				return null;
			}

			@Override
			public int getStatus() {
				return 666;
			}

			@Override
			public StatusType getStatusInfo() {
				return null;
			}


			@Override
			public MultivaluedMap<String, Object> getMetadata() {
				return null;
			}

			@Override
			public MultivaluedMap<String, String> getStringHeaders() {
				return null;
			}

			@Override
			public String getHeaderString(String s) {
				return null;
			}


		};

		Response ret = tws.invoke(jwtToken.getAccessToken(), BaseWebService.SecureType.NORMAL, ti);
		assertTrue(ti.isInvoked);
		assertEquals(tws.sessionId, jwtToken.getAccessToken());
		assertEquals(null, tws.cookieId);
		assertEquals(666, ret.getStatus());

	}

}
