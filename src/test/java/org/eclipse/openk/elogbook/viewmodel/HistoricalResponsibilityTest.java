/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;

import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.eclipse.openk.elogbook.persistence.model.RefBranch;
import org.eclipse.openk.elogbook.persistence.model.RefGridTerritory;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertTrue;

public class HistoricalResponsibilityTest extends ResourceLoaderBase {
    // IMPORTANT TEST!!!
    // Make sure, our Interface produces a DEFINED Json!
    // Changes in the interface will HOPEFULLY crash here!!!

    @Test
    public void testStructureAgainstJson() {
        String json = super.loadStringFromResource("testHistoricalResponsibility.json");
        HistoricalResponsibility hresp = JsonGeneratorBase.getGson().fromJson(json, HistoricalResponsibility.class);
        Date earlyDate = new Date(1000L * 60 * 60 * 24 * 365 * 5);

        //assertNotNull( hresp );
        assertTrue(hresp.getId() == 1);
        assertTrue( hresp.getResponsibleUser().equals("responsibleUser"));
        assertTrue( hresp.getFormerResponsibleUser().equals("formerResponsibleUser"));
        assertTrue( hresp.getCreateUser().equals("createUser"));
        assertTrue( hresp.getModUser().equals("modUser"));
        assertTrue( hresp.getTransactionId() == 2);
        assertTrue( hresp.getRefBranch().getId() == 33);
        assertTrue( hresp.getRefGridTerritory().getId() == 44);
        assertTrue( earlyDate.before(hresp.getCreateDate()));
        assertTrue( earlyDate.before(hresp.getModDate()));
        assertTrue( earlyDate.before(hresp.getTransferDate()));

    }

    @Test
    public void testSetters() {
        Date aDate = new Date(1000L * 60 * 60 * 24 * 365 * 5);

        HistoricalResponsibility hresp = new HistoricalResponsibility();
        hresp.setId(1);
        hresp.setResponsibleUser("responsibleUser");
        hresp.setFormerResponsibleUser("formerResponsibleUser");
        hresp.setCreateUser("createUser");
        hresp.setModUser("modUser");
        hresp.setTransactionId(2);
        hresp.setRefBranch(new RefBranch());
        hresp.setRefGridTerritory(new RefGridTerritory());
        hresp.setCreateDate(aDate);
        hresp.setModDate(aDate);
        hresp.setTransferDate(aDate);
    }
}
