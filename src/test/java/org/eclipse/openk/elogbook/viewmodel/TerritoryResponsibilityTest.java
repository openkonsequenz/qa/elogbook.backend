/**
 * *****************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * <p>
 * http://www.eclipse.org/legal/epl-v10.html
 * <p>
 * *****************************************************************************
 */
package org.eclipse.openk.elogbook.viewmodel;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.elogbook.common.Globals;
import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

public class TerritoryResponsibilityTest extends ResourceLoaderBase {
    // IMPORTANT TEST!!!
    // Make sure, our Interface produces a DEFINED Json!
    // Changes in the interface will HOPEFULLY crash here!!!

    @Test
    public void testStructureAgainstJson() {
        String json = super.loadStringFromResource("testTerritoryResponsibility.json");
        List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase.getGson().fromJson(json,
                new TypeToken<List<TerritoryResponsibility>>() {
        }.getType());

        assertEquals(3, territoryResponsiblityList.size());
        assertEquals(4, territoryResponsiblityList.get(0).getResponsibilityList().size());
        assertEquals("Mannheim", territoryResponsiblityList.get(0).getGridTerritoryDescription());
        assertEquals("responsibleUser1",
                territoryResponsiblityList.get(0).getResponsibilityList().get(0).getResponsibleUser());
        assertEquals("newResponsibleUser",
                territoryResponsiblityList.get(0).getResponsibilityList().get(0).getNewResponsibleUser());
        assertEquals(Globals.ELECTRICITY_MARK,
                territoryResponsiblityList.get(0).getResponsibilityList().get(0).getBranchName());

        assertEquals(45, (int) territoryResponsiblityList.get(2).getResponsibilityList().get(1).getId());

    }

    @Test
    public void testSetters() {
        TerritoryResponsibility territoryResponsibility = new TerritoryResponsibility();
        territoryResponsibility.setGridTerritoryDescription("Ort1");
        List<Responsibility> responsibilityList = new ArrayList<>();
        territoryResponsibility.setResponsibilityList(responsibilityList);

        assertEquals("Ort1", territoryResponsibility.getGridTerritoryDescription());
        assertEquals(0, territoryResponsibility.getResponsibilityList().size());
    }
}
