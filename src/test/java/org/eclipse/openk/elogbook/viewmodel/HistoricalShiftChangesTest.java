/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

public class HistoricalShiftChangesTest extends ResourceLoaderBase {

    @Test
   
    public void TestStructureAgainstJson() {
        String json = super.loadStringFromResource("testHistoricalShiftChanges.json");

        HistoricalShiftChanges shiftChanges = JsonGeneratorBase.getGson().fromJson(json, HistoricalShiftChanges.class);
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, Calendar.JULY, 17);
        Date compareDate = calendar.getTime();
       
        assertNotNull(shiftChanges);
        assertTrue(compareDate.after(shiftChanges.getTransferDateFrom()));
        assertTrue(compareDate.before(shiftChanges.getTransferDateTo()));
       assertEquals(shiftChanges.getHistoricalResponsibilities().size(), 10);
 
    }

    @Test
    public void testSetters() {
        Date now = new Date();
        HistoricalShiftChanges shiftChanges = new HistoricalShiftChanges();
        shiftChanges.setTransferDateFrom(now);
        shiftChanges.setTransferDateTo(now);
        shiftChanges.setHistoricalResponsibilities(new ArrayList<HistoricalResponsibility>());
        
        assertEquals(now, shiftChanges.getTransferDateFrom());
        assertEquals(now, shiftChanges.getTransferDateTo());
        assertTrue(shiftChanges.getHistoricalResponsibilities().isEmpty());
    }
}
