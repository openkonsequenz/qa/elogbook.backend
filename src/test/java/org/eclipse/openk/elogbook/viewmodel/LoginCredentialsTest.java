/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

public class LoginCredentialsTest extends ResourceLoaderBase {

	@Test
	public void testStructureAgainstJson() {
		String json = super.loadStringFromResource("testLoginCredentials.json");
		LoginCredentials lc = JsonGeneratorBase.getGson().fromJson(json, LoginCredentials.class);
		assertEquals(lc.getUserName(), "Carlo");
		assertEquals(lc.getPassword(), "Cottura");
	}

	@Test
	public void testSetters() {
		LoginCredentials lc = new LoginCredentials();
		lc.setPassword("pwd");
		assertTrue("pwd".equals(lc.getPassword()));
		lc.setUserName("usr");
		assertTrue("usr".equals(lc.getUserName()));
	}

}
