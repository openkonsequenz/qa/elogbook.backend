/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;

import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class NotificationFileTest extends ResourceLoaderBase {
    @Test
    public void testStructureAgainstJson() {
        String json = super.loadStringFromResource("testNotificationFile.json");
        NotificationFile notFile = JsonGeneratorBase.getGson().fromJson(json, NotificationFile.class);

        assertTrue(notFile.getFileName().equals("test.csv"));
        assertTrue(notFile.getCreationDate().equals("2017-09-25T12:00:01.635887Z"));
        assertTrue(notFile.getCreator().equals("admin"));
        assertTrue(notFile.getType().equals("csv"));
        assertTrue(notFile.getSize() == 5);
        assertTrue(notFile.getBranchName().equals("S"));
        assertTrue(notFile.getGridTerritoryName().equals("MA"));
        assertTrue(notFile.getNotificationText().equals("This is a simple test text"));

    }

    @Test
    public void testSetters() {
        NotificationFile notFile = new NotificationFile();
        notFile.setFileName("testNotificationFile.json");
        notFile.setCreationDate("2017-09-25T12:00:01.635887Z");
        notFile.setCreator("admin");
        notFile.setType("csv");
        notFile.setSize((long) 5);
        notFile.setBranchName("S");
        notFile.setGridTerritoryName("MA");
        notFile.setNotificationText("This is a simple test text");
    }
}
