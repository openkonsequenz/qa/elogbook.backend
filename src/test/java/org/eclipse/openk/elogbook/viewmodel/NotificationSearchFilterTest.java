/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;


import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;

public class NotificationSearchFilterTest extends ResourceLoaderBase {
    // IMPORTANT TEST!!!
    // Make sure, our Interface produces a DEFINED Json!
    // Changes in the interface will HOPEFULLY crash here!!!

    @Test
    public void TestStructureAgainstJson() {
        String json = super.loadStringFromResource("testNotificationSearchFilter.json");

        NotificationSearchFilter nsf = JsonGeneratorBase.getGson().fromJson(json, NotificationSearchFilter.class);
        Date earlyDate = new Date(1000L * 60 * 60 * 24 * 365 * 5);
        assertNotNull(nsf);
        assertTrue(earlyDate.before(nsf.getDateFrom()));
        assertTrue(earlyDate.before(nsf.getDateTo()));
        assertNotNull(nsf.getResponsibilityFilterList());
        assertTrue(nsf.getResponsibilityFilterList().size() == 2);
        assertTrue(earlyDate.before(nsf.getReminderDate()));
    }

    @Test
    public void testSetters() {
        Date earlyDate = new Date(1000L * 60 * 60 * 24 * 365 * 5);
        Date now = new Date(System.currentTimeMillis());
        NotificationSearchFilter nsf = new NotificationSearchFilter();
        nsf.setDateFrom(earlyDate);
        assertEquals(earlyDate, nsf.getDateFrom());

        nsf.setDateTo(now);
        assertEquals(now, nsf.getDateTo());

        nsf.setResponsibilityFilterList(new ArrayList<>());
        nsf.getResponsibilityFilterList().add(Integer.valueOf(3));
        nsf.getResponsibilityFilterList().add(Integer.valueOf(19));
        nsf.getResponsibilityFilterList().add(Integer.valueOf(22));
        nsf.setReminderDate(now);
        
        nsf.setHistoricalFlag(Boolean.TRUE);
        nsf.setShiftChangeTransactionId(Integer.valueOf(34));

        assertEquals(Integer.valueOf(3), nsf.getResponsibilityFilterList().get(0));
        assertEquals(Integer.valueOf(19), nsf.getResponsibilityFilterList().get(1));
        assertEquals(Integer.valueOf(22), nsf.getResponsibilityFilterList().get(2));
        assertEquals(now, nsf.getReminderDate());
        assertEquals(Boolean.TRUE, nsf.isHistoricalFlag());
        assertEquals(Integer.valueOf(34), nsf.getShiftChangeTransactionId());
    }

	@Test
	public void testHistoricalFlag() {
		NotificationSearchFilter nsf = new NotificationSearchFilter();
		assertNotNull(nsf.isHistoricalFlag());
		assertFalse(nsf.isHistoricalFlag());
	}
}
