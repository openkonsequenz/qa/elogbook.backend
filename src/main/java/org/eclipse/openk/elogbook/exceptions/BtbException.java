/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.exceptions;

public abstract class BtbException extends Exception { // NOSONAR
    public BtbException() {
        super();
    }

    public BtbException(String message) {
        super(message);
    }

    public BtbException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public abstract int getHttpStatus();
}
