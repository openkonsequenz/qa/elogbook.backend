/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.openk.elogbook.exceptions.BtbBadRequest;
import org.eclipse.openk.elogbook.exceptions.BtbException;

public class ListTypeMapper {
	private static Map<String, Notification.ListType> listTypeMap = new HashMap<>();

	static {
		listTypeMap.put("OPEN", Notification.ListType.OPEN);
		listTypeMap.put("ALL", Notification.ListType.ALL);
		listTypeMap.put("PAST", Notification.ListType.PAST);
		listTypeMap.put("CURRENT", Notification.ListType.CURRENT);
		listTypeMap.put("FUTURE", Notification.ListType.FUTURE);
	}

	private ListTypeMapper() {
	}

	public static synchronized Notification.ListType listTypeFromString(String listType)
			throws BtbException {
		String noti = (listType != null) ? listType.toUpperCase() : "";

		if (listTypeMap.containsKey(noti)) {
			return listTypeMap.get(noti);
		}

		throw new BtbBadRequest("Unknown parameter value: " + listType);
	}
}
