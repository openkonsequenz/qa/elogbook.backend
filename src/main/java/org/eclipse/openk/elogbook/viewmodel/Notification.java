/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Notification {

    public enum ListType { ALL, PAST, CURRENT, OPEN, FUTURE }

    private Integer id;
    private Integer incidentId;
    private Integer version;
    private boolean selected;
    private String status;
    private Date beginDate;
    private String notificationText;
    private String freeText;
    private String freeTextExtended;
    private String responsibilityForwarding;
    private UUID responsibilityForwardingUuid;
    private String responsibilityControlPoint;
    private Date reminderDate;
    private Date futureDate;
    private Date expectedFinishDate;
    private Date finishedDate;
    private String createUser;
    private Date createDate;
    private Date modDate;
    private String modUser;

    private Integer fkRefBranch;
    private Integer fkRefNotificationStatus;
    private Integer fkRefNotificationPriority;
    private Integer fkRefGridTerritory;

    private Boolean adminFlag;
    private String type;

    private List<Notification> notificationList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public String getNotificationText() {
        return notificationText;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    public String getFreeTextExtended() {
        return freeTextExtended;
    }

    public void setFreeTextExtended(String freeTextExtended) {
        this.freeTextExtended = freeTextExtended;
    }

    public String getResponsibilityForwarding() {
        return responsibilityForwarding;
    }

    public void setResponsibilityForwarding(String responsibilityForwarding) {
        this.responsibilityForwarding = responsibilityForwarding;
    }

    public String getResponsibilityControlPoint() {
        return responsibilityControlPoint;
    }

    public void setResponsibilityControlPoint(String responsibilityControlPoint) {
        this.responsibilityControlPoint = responsibilityControlPoint;
    }

    public Date getReminderDate() {
        return reminderDate;
    }

    public void setReminderDate(Date reminderDate) {
        this.reminderDate = reminderDate;
    }

    public Date getFutureDate() {
        return futureDate;
    }

    public void setFutureDate(Date futureDate) {
        this.futureDate = futureDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getExpectedFinishDate() {
        return expectedFinishDate;
    }

    public void setExpectedFinishDate(Date expectedFinishDate) {
        this.expectedFinishDate = expectedFinishDate;
    }

    public Date getFinishedDate() {
        return finishedDate;
    }

    public void setFinishedDate(Date finishedDate) {
        this.finishedDate = finishedDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String creator) {
        this.createUser = creator;
    }

    public Integer getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(Integer incidentId) {
        this.incidentId = incidentId;
    }

    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    public String getModUser() {
        return modUser;
    }

    public void setModUser(String modUser) {
        this.modUser = modUser;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getFkRefBranch() {
        return fkRefBranch;
    }

    public void setFkRefBranch(Integer fkRefBranch) {
        this.fkRefBranch = fkRefBranch;
    }

    public Integer getFkRefNotificationStatus() {
        return fkRefNotificationStatus;
    }

    public void setFkRefNotificationStatus(Integer fkRefNotificationStatus) {
        this.fkRefNotificationStatus = fkRefNotificationStatus;
    }

	public Integer getFkRefGridTerritory() {
		return fkRefGridTerritory;
	}

	public void setFkRefGridTerritory(Integer fkRefGridTerritory) {
		this.fkRefGridTerritory = fkRefGridTerritory;
	}

	public Boolean isAdminFlag() {
		return adminFlag;
	}

	public void getAdminFlag(Boolean adminFlag) {
		this.adminFlag = adminFlag;
	}

    public List<Notification> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<Notification> notificationList) {
        this.notificationList = notificationList;
    }

    public Integer getFkRefNotificationPriority() {
        return fkRefNotificationPriority;
    }

    public void setFkRefNotificationPriority(Integer fkRefNotificationPriority) {
        this.fkRefNotificationPriority = fkRefNotificationPriority;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UUID getResponsibilityForwardingUuid() {
        return responsibilityForwardingUuid;
    }

    public void setResponsibilityForwardingUuid(UUID responsibilityForwardingUuid) {
        this.responsibilityForwardingUuid = responsibilityForwardingUuid;
    }
}
