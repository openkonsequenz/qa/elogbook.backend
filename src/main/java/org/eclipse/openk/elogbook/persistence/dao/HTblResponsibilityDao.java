/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.eclipse.openk.elogbook.exceptions.BtbInternalServerError;
import org.eclipse.openk.elogbook.persistence.model.HTblResponsibility;


public class HTblResponsibilityDao extends GenericDaoJpa<HTblResponsibility, Integer> {

	/** find only HTblResponsibilities starting with transfer date */
	private static final String TRANSFER_DATE_FROM = "transferDateFrom";
	/** find only HTblResponsibilities ending with transfer date */
	private static final String TRANSFER_DATE_TO = "transferDateTo";

    public HTblResponsibilityDao() {
        super();
    }

    public HTblResponsibilityDao(EntityManager em) {
        super(em);
    }


    @SuppressWarnings("unchecked")
    public List<HTblResponsibility> getHistoricalResponsibilitiesByTransactionId(Integer transactionId) {

        try {
            String selectString = "from " + HTblResponsibility.class.getSimpleName() + " v where v.transactionId = :transactionId";
            Query q = getEM().createQuery(selectString);
            q.setParameter("transactionId", transactionId);
            return refreshModelCollection((List<HTblResponsibility>) q.getResultList());
        } catch (Throwable t) { //NOSONAR
            LOGGER.error(t.getMessage());
            return new ArrayList<>();
        }
    }


	/**
   * Find the "last used" (max) transaction id of hTblResponsibilites.
   *
   * @return the max transaction_id of htblResponsibilities
   */
  public Integer getLastTransactionId() throws BtbInternalServerError {

    try {
      String selectString = "FROM HTblResponsibility n WHERE n.transactionId = (SELECT MAX(t2.transactionId) FROM HTblResponsibility t2)";
      Query q = getEM().createQuery(selectString);
      q.setMaxResults(1);
      return ((HTblResponsibility) q.getSingleResult()).getTransactionId();
    } catch (NoResultException nre) {
      return 0;
    } catch (Throwable t) { //NOSONAR
      LOGGER.error(t.getMessage());
      throw new BtbInternalServerError("Error getting last transaction id of htbl_responsibility");
    }
  }

	/**
	 * Find the all historical responsibilities in the period specified by start
	 * date and end date and return them in a list.
	 *
	 * @param transferDateFrom
	 *            period - begin
	 * @param transferDateTo
	 *            period - end
	 * @return the list containing the historical responsibilities in the
	 *         specified period
	 */
	@SuppressWarnings("unchecked")
	public List<HTblResponsibility> findHTblResponsibilitiesInPeriod(Date transferDateFrom, Date transferDateTo)
			throws BtbInternalServerError {
		try {
			String selectString = "from HTblResponsibility t "
					+ "WHERE t.transferDate >= :" + TRANSFER_DATE_FROM
					+ " and t.transferDate <= :" + TRANSFER_DATE_TO
					+ " ORDER BY t.transferDate DESC";


			Query q = getEM().createQuery(selectString);
			q.setParameter(TRANSFER_DATE_FROM, transferDateFrom);
			q.setParameter(TRANSFER_DATE_TO, transferDateTo);
			List<HTblResponsibility> resultList = q.getResultList();
			List<HTblResponsibility> distinctResultList  = new LinkedList<>();

			// build up distinct list regarding the fields in "hashItDistinct"
			Set<String> uniqueSet = new HashSet<>();
			for( HTblResponsibility rp: resultList) {
				String hash = hashItDistinct(rp);
				if(!uniqueSet.contains(hash)) {
					uniqueSet.add(hash);
					distinctResultList.add(rp);
				}
			}
			return distinctResultList;
		} catch (Throwable t) { // NOSONAR
			LOGGER.error(t.getMessage());
			throw new BtbInternalServerError("Error finding responsibilities in Period");
		}
	}

	private String hashItDistinct( HTblResponsibility item ) {
		return String.valueOf(item.getTransactionId())
				+ "@"	+ item.getTransferDate().getTime()
				+ "@" + item.getResponsibleUser() + "@"
				+ item.getFormerResponsibleUser() + "@";
	}

	/**
	 * Find all shift changes with the transaction id, given as parameter.
	 *
	 * @param shiftTransactionId
	 *            the transaction id to search for historical responsibilities
	 * @return the list containing the matching responsibilities
	 */
	@SuppressWarnings("unchecked")
	public List<HTblResponsibility> findResponsibilitiesByTransactionId(Integer shiftTransactionId)
			throws BtbInternalServerError {
		try {
			StringBuilder sB = new StringBuilder(
					"FROM HTblResponsibility h WHERE h.transactionId = :shiftTransactionId");
			Query query = getEM().createQuery(sB.toString());
			query.setParameter("shiftTransactionId", shiftTransactionId);
			List<HTblResponsibility> hTblResponsibilities = (List<HTblResponsibility>) query.getResultList();
			refreshModelCollection(hTblResponsibilities);

			LOGGER.debug("Results for shift changes with transaction id " + shiftTransactionId);
			for (HTblResponsibility hTblResponsibility : hTblResponsibilities) {
				LOGGER.debug("id: " + hTblResponsibility.getId() + ", foreign key Branch: "
						+ hTblResponsibility.getRefBranch().getId() + ", foreign key grid territory: "
						+ hTblResponsibility.getRefGridTerritory().getId());
			}
			return hTblResponsibilities;
		} catch (Throwable t) { // NOSONAR
			LOGGER.error(t.getMessage());
			throw new BtbInternalServerError("Error in findShiftChangeByTransactionId occured.");
		}
	}
}

