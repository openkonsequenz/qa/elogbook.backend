/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the ref_notification_status database table.
 *
 */
@Entity
@Table(name="REF_NOTIFICATION_STATUS")
@NamedQuery(name="RefNotificationStatus.findAll", query="SELECT r FROM RefNotificationStatus r")
public class RefNotificationStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
	private Integer id;

	@Column(name = "name")
	private String name;

	public RefNotificationStatus() {
		// default constructor
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}