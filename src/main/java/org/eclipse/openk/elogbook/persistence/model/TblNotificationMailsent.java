/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;


/**
 * The persistent class for the tbl_notfication_mail_sent database table.
 *
 */
@Entity
@Table(name="TBL_NOTIFICATION_MAILSENT")
@NamedQuery(name="TblNotificationMailsent.findAll", query="SELECT t FROM TblNotificationMailsent t")
public class TblNotificationMailsent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_NOTIFICATION_MAILSENT_SEQ")
	@SequenceGenerator(name = "TBL_NOTIFICATION_MAILSENT_SEQ", sequenceName = "TBL_NOTIFICATION_MAILSENT_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Integer id;


	@Column(name="incident_id")
	private Integer incidentId;

	@Column(name="mail_sent")
	private boolean mailSent;

	@Column(name = "date_mail_sent")
	private Timestamp dateMailSent;


	public TblNotificationMailsent() {
		// default constructor
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isMailSent() {
		return mailSent;
	}

	public void setMailSent(boolean mailSent) {
		this.mailSent = mailSent;
	}

	public Timestamp getDateMailSent() {
		return dateMailSent;
	}

	public void setDateMailSent(Timestamp dateMailSent) {
		this.dateMailSent = dateMailSent;
	}

	public Integer getIncidentId() {
		return incidentId;
	}

	public void setIncidentId(Integer incidentId) {
		this.incidentId = incidentId;
	}
}