/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * The persistent class for the ref_branch database table.
 *
 */
@Entity
@Table(name="TBL_USER_SETTINGS")
@NamedQuery(name="TblUserSettings.findAll", query="SELECT t FROM TblUserSettings t")
public class TblUserSettings implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_USER_SETTINGS_ID_SEQ")
	@SequenceGenerator(name = "TBL_USER_SETTINGS_ID_SEQ", sequenceName = "TBL_USER_SETTINGS_ID_SEQ", allocationSize = 1)
  @Column(name = "id")
	private Integer id;

	@Column(name = "username")
	private String username;

	@Column(name = "setting_type")
	private String settingType;

	@Column(name = "value")
	private String value;

	public TblUserSettings() {
		// default constructor
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSettingType() {
		return settingType;
	}

	public void setSettingType(String settingType) {
		this.settingType = settingType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}