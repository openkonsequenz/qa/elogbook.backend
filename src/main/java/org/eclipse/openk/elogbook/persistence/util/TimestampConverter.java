/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.util;

import java.sql.Timestamp;
import java.util.Calendar;

public class TimestampConverter {
    private TimestampConverter() { }

    public static Timestamp getTimestampWithTime(Timestamp tsorg, int hours, int minutes, int seconds) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(tsorg.getTime());
        Calendar c2 = Calendar.getInstance();
        c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), hours, minutes, seconds);
        return new Timestamp(c2.getTimeInMillis());
    }
}
