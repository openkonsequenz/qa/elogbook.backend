/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.dao;

import javax.persistence.EntityManager;
import org.eclipse.openk.elogbook.persistence.model.RefNotificationStatus;


public class RefNotificationStatusDao extends GenericDaoJpa<RefNotificationStatus, Long> {
    public RefNotificationStatusDao() {
        super();
    }

    public RefNotificationStatusDao(EntityManager em) {
        super(em);
    }

}
