/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.elogbook.email;


import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static jakarta.mail.Message.RecipientType.CC;
import static jakarta.mail.Message.RecipientType.TO;
import static jakarta.mail.Part.INLINE;

public class Email {

  private static final Logger log = LogManager.getLogger(Email.class.getName());

  private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

  private static final Pattern PATTERN = Pattern.compile(EMAIL_PATTERN);

  private static final String MAIL_ENCODING = "UTF-8";

  private final Properties props = new Properties();
  private final MimeMultipart multipart = new MimeMultipart();
  private final MimeMessage message;
  private final boolean htmlMail;


  public Email(String smtpHost, String port) {
    this(smtpHost, port, true);
  }

  public Email(String smtpHost, String port, boolean htmlMail) {
    props.put("mail.smtp.auth", "false");
    props.put("mail.smtp.host", smtpHost);
    props.put("mail.smtp.port", port);
    this.htmlMail = htmlMail;
    Session session = Session.getInstance(props);
    message = new MimeMessage(session);
  }

  public void addText(String text) throws MessagingException {
    MimeBodyPart textBody = new MimeBodyPart();
    if (htmlMail) {
      textBody.setText(text, MAIL_ENCODING, "html");
    } else {
      textBody.setText(text, MAIL_ENCODING);
    }
    textBody.setDisposition(INLINE);
    multipart.addBodyPart(textBody);
  }

  public void setFrom(String emailSender) throws MessagingException {
    try {
      boolean validRecipient = validateEmailAddress(emailSender);
      if (!validRecipient) {
        log.error("Invalid email-addresse for sender: " + emailSender);
        throw new MessagingException();
      }
      message.setFrom(new InternetAddress(emailSender));
    } catch (MessagingException e) {
      log.error("setFrom MessagingException caught()", e);
      throw e;
    }
  }

  public void addRecipients(List<String> recipientList) throws MessagingException {
    String recipientListSeperated = String.join(",", recipientList);
    addRecipient(recipientListSeperated);
  }

  public void addRecipient(String recipients) throws MessagingException {
    addRecipient(recipients, TO);
  }

  public void addCC(String recipients) throws MessagingException {
    addRecipient(recipients, CC);
  }

  public void addRecipient(String recipients, Message.RecipientType recipientType) throws MessagingException {
    try {
      String checkedRecipients;
      if(recipients != null) {
        recipients = recipients.trim();
        recipients = recipients.replace(";", ",");
        String[] recipientsSplit = recipients.split(",");

        validateEmailAddress(recipientsSplit);
        checkedRecipients = String.join(",", recipientsSplit);

        //Array of recipients must include only unique recipients => use of a Set
        InternetAddress[] mailAddresses = InternetAddress.parse(checkedRecipients);
        Set<InternetAddress> mailAddressesUniqueSet = new HashSet<>(Arrays.asList(mailAddresses));
        InternetAddress[] mailAddressesFiltered = mailAddressesUniqueSet.toArray(new InternetAddress[0]);

        message.addRecipients(recipientType, mailAddressesFiltered);
        log.debug("Email-addresse successfully set: " + checkedRecipients);
      }
    } catch (MessagingException e) {
      log.error("addRecipient MessagingException caught()", e);
      throw e;
    }
  }

  private void validateEmailAddress(String[] recipientsSplit) throws MessagingException {
    for (String recipient : recipientsSplit) {
      if (!validateEmailAddress(recipient.trim())) {
        log.error("Invalid email-addresse: " + recipient);
        throw new MessagingException();
      }
    }
  }

  public void setSubject(String subjectText) {
    try {
      message.setSubject(subjectText, MAIL_ENCODING);
    } catch (MessagingException e) {
      log.error("MessagingException caught()", e);
    }
  }

  public void setContent() {
    try {
      message.setContent(multipart);
    } catch (MessagingException e) {
      log.error("MessagingException caught()", e);
    }
  }

  public boolean send() {
    log.debug("send() is called");
    boolean ret = false;

    try {
      setContent();
      Transport.send(message);
      log.debug("send() finished");
      ret = true;
    } catch (MessagingException e) {
      log.error("send MessagingException caught()", e);
    }
    return ret;
  }

  public MimeMessage getMessage() {
    return message;
  }

  public static boolean validateEmailAddress(final String hex) {

    Matcher matcher = PATTERN.matcher(hex);
    return matcher.matches();

  }
}
