package org.eclipse.openk.elogbook.common.timer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.controller.EmailService;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class PresenceCheckJob implements Job {

    private static final Logger LOGGER = LogManager.getLogger(PresenceCheckJob.class.getName());

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        LOGGER.debug("Quartz: PresenceCheckJob called");

        try {
            EmailService.getInstance().sendPresenceCheckReminderMail();
            LOGGER.debug("Quartz: PresenceCheckJob finished");
        } catch (BtbException e) {
            LOGGER.error("Error while executing PresenceCheckJob", e);
        }
    }

}
