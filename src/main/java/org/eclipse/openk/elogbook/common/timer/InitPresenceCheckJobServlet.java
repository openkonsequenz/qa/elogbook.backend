package org.eclipse.openk.elogbook.common.timer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.common.BackendConfig;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class InitPresenceCheckJobServlet extends HttpServlet {

    private static final long serialVersionUID = -7802116179901471578L;

    private static final Logger LOGGER = LogManager.getLogger(InitPresenceCheckJobServlet.class.getName());

    @Override
    public void init() throws ServletException {
        String cronExpression = BackendConfig.getInstance().getCronEmailPresenceCheck();
        boolean isJobEnabled = BackendConfig.getInstance().isEnableEmailPresenceCheckJob();
        if (isJobEnabled) {
            LOGGER.info("Init timer: 'Check presence job' triggered with cronExpression: " + cronExpression);

            Trigger trigger = TriggerBuilder.newTrigger().withIdentity("CheckPresenceTrigger", "CheckPresenceTriggerGroup1")
                        .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression)).build();

            Scheduler scheduler;

            try {
                JobDetail checkUserStatusJob = JobBuilder.newJob(PresenceCheckJob.class)
                        .withIdentity("checkPresenceJob", "checkPresenceJobGroup").build();
                scheduler = new StdSchedulerFactory().getScheduler();
                scheduler.start();
                scheduler.scheduleJob(checkUserStatusJob, trigger);
            } catch (SchedulerException e) {
                LOGGER.error("SchedulerException caught", e);
            }
        } else {
            LOGGER.info("'Check presence job' is disabled");
        }

    }

}
