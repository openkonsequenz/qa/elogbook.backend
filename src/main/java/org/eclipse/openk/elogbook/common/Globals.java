/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common;


public final class Globals {
    public static final String OK_STRING = "OK";
    public static final String SESSION_TOKEN_TAG = "X-XSRF-TOKEN";
    public static final String SESSION_COOKIE_TOKEN_TAG = "COOKIESESSION-TOKEN";
    public static final int MAX_CREDENTIALS_LENGTH = 500;
    public static final int MAX_NOTIFICATION_LENGTH = 16384;

    //For Branches: for later use in a configurable json
    public static final String ELECTRICITY_MARK = "S";
    public static final String GAS_MARK = "G";
    public static final String DISTRICT_HEAT_MARK = "F";
    public static final String WATER_MARK = "W";

    //Central fault monitoring (Zentrale Störmeldestelle (ZSM))
    public static final String CFM_MARK = "Z";

    //Errormessage
    public static final String DATA_OUTDATED = "Data is outdated!";

    //Suggestion: An input field on search mask as better solution? Can only be adapted by code changes.
    public static final int FAST_SEARCH_NUMBER_OF_DAYS_BACK = -200;

    //KeyCloak configuration
    public static final String KEYCLOAK_AUTH_TAG = "Authorization";
    public static final String KEYCLOAK_ROLE_SUPERUSER = "elogbook-superuser";
    public static final String KEYCLOAK_ROLE_NORMALUSER = "elogbook-normaluser";

    public static final String HEADER_JSON_UTF8 = "application/json; charset=utf-8";

    public static final String NOTIFICATION_TYPE_PRESENCE_CHECK = "presence-check";


    private Globals() {}

}
