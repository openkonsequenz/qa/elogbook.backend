/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;


import com.google.gson.reflect.TypeToken;

import org.eclipse.openk.elogbook.common.BackendConfig;
import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.communication.RestServiceWrapper;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.persistence.dao.AutoCloseEntityManager;
import org.eclipse.openk.elogbook.persistence.dao.EntityHelper;
import org.eclipse.openk.elogbook.persistence.dao.TblNotificationDao;
import org.eclipse.openk.elogbook.viewmodel.contactbasedata.AssignmentModulContactDto;
import org.eclipse.openk.elogbook.viewmodel.contactbasedata.ContactTupel;
import org.eclipse.openk.elogbook.viewmodel.contactbasedata.VwDetailedContact;
import org.eclipse.openk.elogbook.viewmodel.contactbasedata.VwDetailedContactPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class ContactBaseDataManager {
    private static final Logger LOGGER = LogManager.getLogger(ContactBaseDataManager.class.getName());


    private static final ContactBaseDataManager INSTANCE = new ContactBaseDataManager();

    private ContactBaseDataManager() {
    }

    public static ContactBaseDataManager getInstance() {
        return INSTANCE;
    }

    public List<ContactTupel> getUserContacts(String token) throws BtbException {
        RestServiceWrapper restServiceWrapper = new RestServiceWrapper(BackendConfig.getInstance().getContactBaseDataApiUrl(), false);
        String response = restServiceWrapper.performGetRequest("contacts?size=2000",token);
        VwDetailedContactPage vwDetailedContactPage = JsonGeneratorBase.getGson().fromJson(response,
                VwDetailedContactPage.class);
        return vwDetailedContactPage.getContent().stream().map(this::mapFromViewObj ).collect(Collectors.toList());
    }

    public List<AssignmentModulContactDto> getAssignedModulsForContact(String token, UUID contactUuid) throws BtbException {
        RestServiceWrapper restServiceWrapper = new RestServiceWrapper(BackendConfig.getInstance().getContactBaseDataApiUrl(), false);
        String response = restServiceWrapper.performGetRequest("contacts/"+contactUuid+"/assignments",token);
        return JsonGeneratorBase.getGson().fromJson(response
                , new TypeToken<List<AssignmentModulContactDto>>() {}.getType());
    }

    public void postAssignedModulsForContact(String token, UUID contactUuid) throws BtbException {
        AssignmentModulContactDto modulContactDto = new AssignmentModulContactDto();
        modulContactDto.setContactId(contactUuid);
        modulContactDto.setModulName(BackendConfig.getInstance().getElogbookModuleName());
        String data = JsonGeneratorBase.getGson().toJson(modulContactDto);
        RestServiceWrapper restServiceWrapper = new RestServiceWrapper(BackendConfig.getInstance().getContactBaseDataApiUrl(), false);
        restServiceWrapper.performPostRequest("contacts/"+contactUuid+"/assignments", token, data);
    }

    public void synchronizeContacts(String token) {
        LOGGER.debug("synchronizeContacts is called");
        EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
        try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
            em.getTransaction().begin();
            TblNotificationDao notifDao = new TblNotificationDao(em);
            Map<String, String> uuid2NameMap = getUserContacts(token).stream()
                    .collect(Collectors.toMap( x -> x.getContactUuid().toString(), ContactTupel::getContactName));
            notifDao.getDistinctResponsibilityForwardingUuid()
                        .forEach( contactUuid -> notifDao.updateResponsibilityForwardingByContactUuid( contactUuid, uuid2NameMap.getOrDefault(contactUuid, "/gelöscht/")));
            em.getTransaction().commit();

        } catch (BtbException btbException) {
            LOGGER.error("error in synchronizeContacts", btbException);
        } finally {
            LOGGER.debug("synchronizeContacts is finished");
        }
    }


    private ContactTupel mapFromViewObj( VwDetailedContact vwDetailedContact ) {
        String userName = vwDetailedContact.getName();
        if (userName != null) {
            userName = userName.replace("," ,"");
        }
        ContactTupel ct = new ContactTupel();
        ct.setContactName(userName);
        ct.setContactUuid(vwDetailedContact.getUuid());
        return ct;

    }

}
