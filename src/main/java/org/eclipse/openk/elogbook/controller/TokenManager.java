/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.auth2.model.JwtPayload;
import org.eclipse.openk.elogbook.auth2.util.JwtHelper;
import org.eclipse.openk.elogbook.common.BackendConfig;
import org.eclipse.openk.elogbook.common.Globals;
import org.eclipse.openk.elogbook.communication.RestServiceWrapper;
import org.eclipse.openk.elogbook.controller.BaseWebService.SecureType;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.exceptions.BtbForbidden;

public class TokenManager {
    private static final Logger LOGGER = LogManager.getLogger(TokenManager.class.getName());

    private static final TokenManager INSTANCE = new TokenManager();

    private TokenManager() {
    }

    public static TokenManager getInstance() {
        return INSTANCE;
    }

    public void logout(String token) throws BtbException {
        RestServiceWrapper restServiceWrapper = new RestServiceWrapper(BackendConfig.getInstance().getPortalBaseURL(), false);
        restServiceWrapper.performGetRequest("logout",token);
    }

    public void checkAut(String token) throws BtbException {
        RestServiceWrapper restServiceWrapper = new RestServiceWrapper(BackendConfig.getInstance().getPortalBaseURL(), false);
        restServiceWrapper.performGetRequest("checkAuth",token);
    }

    public void checkAutLevel(String token, BaseWebService.SecureType secureType) throws BtbForbidden {
        JwtPayload jwtPayload = JwtHelper.getJwtPayload(token);

        if (jwtPayload != null && secureType == SecureType.NORMAL && !(jwtPayload.getRealmAccess()
            .isInRole(Globals.KEYCLOAK_ROLE_NORMALUSER) || jwtPayload.getRealmAccess()
            .isInRole(Globals.KEYCLOAK_ROLE_SUPERUSER))){
            LOGGER.warn("Security level not sufficent.");
            throw new BtbForbidden("insufficent rights");
        } else if (jwtPayload != null && secureType == BaseWebService.SecureType.HIGH &&
            !jwtPayload.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_SUPERUSER)){
            LOGGER.warn("Security level not sufficent!");
            throw new BtbForbidden("insufficent rights");
        }
    }

}
