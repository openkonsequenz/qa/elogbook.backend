/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/

package org.eclipse.openk.elogbook.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.auth2.model.JwtToken;
import org.eclipse.openk.elogbook.auth2.model.KeyCloakUser;
import org.eclipse.openk.elogbook.auth2.util.JwtHelper;
import org.eclipse.openk.elogbook.common.BackendConfig;
import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.mapper.KeyCloakUserMapper;
import org.eclipse.openk.elogbook.communication.RestServiceWrapper;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.exceptions.BtbInternalServerError;
import org.eclipse.openk.elogbook.exceptions.BtbUnauthorized;
import org.eclipse.openk.elogbook.persistence.dao.AutoCloseEntityManager;
import org.eclipse.openk.elogbook.persistence.dao.EntityHelper;
import org.eclipse.openk.elogbook.persistence.dao.TblNotificationDao;
import org.eclipse.openk.elogbook.persistence.dao.TblUserSettingsDao;
import org.eclipse.openk.elogbook.persistence.model.TblUserSettings;
import org.eclipse.openk.elogbook.viewmodel.LoginCredentials;
import org.eclipse.openk.elogbook.viewmodel.UserAuthentication;
import org.eclipse.openk.elogbook.viewmodel.contactbasedata.AssignmentModulContactDto;
import org.eclipse.openk.elogbook.viewmodel.contactbasedata.ContactTupel;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class BackendControllerUser {
	
	private static final Logger LOGGER = LogManager.getLogger(BackendControllerUser.class.getName());

	private static final String DEFAULT_SETTING_TYPE = "<default>";
	
	private final InputDataValuator inputDataValuator = new InputDataValuator();

	public List<UserAuthentication> getUsers(String accesstoken) throws BtbException {
		LOGGER.debug("getUsers() is called");
		RestServiceWrapper restServiceWrapper = new RestServiceWrapper(BackendConfig.getInstance().getPortalBaseURL(), false);

		String url = "usersForRole/"+BackendConfig.getInstance().getApplicationAccessRole();
		String keyCloakUserjson = restServiceWrapper.performGetRequest(url, accesstoken);
		List<KeyCloakUser> keyCloakUserList = JwtHelper.getUserListFromJson(keyCloakUserjson);

		List<UserAuthentication> userAuthenticationList = KeyCloakUserMapper.mapFromKeyCloakUserList(keyCloakUserList);
		LOGGER.debug("getUsers() succeeded.");
		return userAuthenticationList;
	}

	public String portalLogin(String user, String pwd) throws BtbException {
		LOGGER.debug("portalLogin is called");
		String credentials = "{ \"userName\": \""+user+"\", \"password\": \""+pwd+"\"}";
		RestServiceWrapper restServiceWrapper = new RestServiceWrapper(BackendConfig.getInstance().getPortalBaseURL(), false);
		String url = "login";
		JwtToken jwt = JsonGeneratorBase.getGson().fromJson( restServiceWrapper.performPostRequest(url, "", credentials), JwtToken.class );

		return jwt.getAccessToken();

	}

	public UserAuthentication authenticate(String credentials) throws BtbException {
		LOGGER.debug("authenticate() is called");

		// valuator will throw an exception if not valid
		inputDataValuator.checkCredentials(credentials);

		LoginCredentials loginCredentials = JsonGeneratorBase.getGson().fromJson(credentials, LoginCredentials.class);

		UserAuthentication ret = null;
		for (UserAuthentication uatmp : getUsers("")) {
			            String existedUsername = uatmp.getUsername().toLowerCase();
            String inputUsername = loginCredentials.getUserName().toLowerCase();

            String exitedPassword = uatmp.getPassword();
            String inputPassword = loginCredentials.getPassword();

            if( existedUsername.equals(inputUsername) && exitedPassword.equals(inputPassword)){
				ret = uatmp;
				break;
			}
		}

		if (ret == null) {
			throw new BtbUnauthorized("Unknown User/Password");
		}

		LOGGER.debug("authenticate() succeeded.");
		return ret;
	}

	public List<ContactTupel> getUserSuggestionsFromContactdatabase(String token) throws BtbException {
		LOGGER.debug("getUserSuggestionsFromContactdatabase() is called");
		List<ContactTupel> userContacts = ContactBaseDataManager.getInstance().getUserContacts(token);
		LOGGER.debug("getUserSuggestionsFromContactdatabase() is finished");
		return userContacts;
	}

	public void sentTestMail() throws BtbException {
		LOGGER.debug("sentTestMail() is called");
		EmailService.getInstance().sendPresenceCheckReminderMail();
		LOGGER.debug("sentTestMail() is finished");
	}

	public List<String> getAssignedUserSuggestions() throws BtbException {
		LOGGER.debug("getAssignedUserSuggestions() is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			TblNotificationDao dao = new TblNotificationDao(em);
			return dao.getAssignedUserSuggestions();
		} finally {
			LOGGER.debug("getAssignedUserSuggestions() is finished");
		}

	}


	public void synchronizeContacts() throws BtbException {
		LOGGER.debug("synchronizeContacts() is called");

		try {
			ContactBaseDataManager.getInstance().synchronizeContacts(portalLogin(
					BackendConfig.getInstance().getContactServiceTechUser(),
					BackendConfig.getInstance().getContactServiceTechUserPwd()
			));
		} finally {
			LOGGER.debug("synchronizeContacts() is finished");
		}

	}
	public String getUserSettings( String modUser ) throws BtbInternalServerError {
		LOGGER.debug("getUserSettings() is called");
		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			TblUserSettingsDao dao = new TblUserSettingsDao(em);
			try {
				Optional<TblUserSettings> us =  dao.getSettingsForUser(modUser, DEFAULT_SETTING_TYPE);
				return us.isPresent() ? us.get().getValue() : "{}";
			} catch (Exception e) {
				LOGGER.error( "Error loading usersettings" );
				throw( new BtbInternalServerError("Error loading usersettings"));
			}
		} finally {
			LOGGER.debug("getUserSettings() is finished");
		}
	}

	public boolean storeUserSettings( String settingsToSave, String modUser ) throws BtbInternalServerError {
		LOGGER.debug("storeUserSettings() is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			em.getTransaction().begin();
			TblUserSettingsDao dao = new TblUserSettingsDao(em);

			TblUserSettings userSettingsToStore = dao.getSettingsForUser(modUser, DEFAULT_SETTING_TYPE)
														.orElse(new TblUserSettings());

			userSettingsToStore.setSettingType(DEFAULT_SETTING_TYPE);
			userSettingsToStore.setUsername(modUser);
			userSettingsToStore.setValue(settingsToSave);
			try {
				dao.persistInTx(userSettingsToStore);
				em.getTransaction().commit();
				return true;
			} catch (Exception e) {
				LOGGER.error( "Error storing usersettings" );
				throw( new BtbInternalServerError("Error storing usersettings"));
			}
		} finally {
			LOGGER.debug("storeUserSettings() is finished");
		}
	}

}
