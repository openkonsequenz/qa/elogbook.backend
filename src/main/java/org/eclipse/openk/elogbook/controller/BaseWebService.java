/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;

import org.apache.http.HttpStatus;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.auth2.model.JwtPayload;
import org.eclipse.openk.elogbook.auth2.util.JwtHelper;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.exceptions.BtbExceptionMapper;

import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseWebService {
    public enum SecureType {NONE, NORMAL, HIGH}
    private final Map<String, Long> currentTimeMeasures = new HashMap<>();
    private final Logger logger;

    private static final boolean DEVELOP_MODE;
    private static final String LET_ME_IN = "LET_ME_IN";

    static {
        // determine static VersionInfo
        String versionString = BaseWebService.class
                .getPackage()
                .getImplementationVersion();
        DEVELOP_MODE =  versionString == null // <-- during unit tests
                || versionString.toUpperCase().contains("DEVELOP")
                || versionString.contains("SNAPSHOT");
    }

    protected BaseWebService(Logger logger) {
        this.logger = logger;
    }

    protected abstract void assertAndRefreshToken(String token, SecureType secureType) throws BtbException;

    private void startProcessing(String methodName) {
        Long lStartingTime = System.currentTimeMillis();
        String lookupName = buildLookupId(methodName);
        currentTimeMeasures.put(lookupName, lStartingTime);

        logger.debug(this.getClass().getName() + "." + lookupName + ": Start processing...");
    }

    protected AutoCloseable perform(final String methodName) {
        startProcessing(methodName);
        return () -> endProcessing(methodName);
    }

    private String buildLookupId(String func) {
        return func + "@@" + Thread.currentThread().getId();
    }

    private void endProcessing(String methodName) {
        String lookupName = buildLookupId(methodName);
        if (currentTimeMeasures.containsKey(lookupName)) {
            Long lStartingTime = currentTimeMeasures.get(lookupName);
            currentTimeMeasures.remove(lookupName);
            logger.debug(
                this.getClass().getName() + "." + lookupName + ": Finished processing in " +
                    (System.currentTimeMillis() - lStartingTime) + " ms");
        }

    }

    protected Response invoke(String token, SecureType secureType, BackendInvokable invokable)
    {
        try (AutoCloseable ignored = perform(invokable.getClass().getName() + ".Invoke()")) { // NOSONAR
            if (secureType != SecureType.NONE) {
                JwtPayload jwtPayload = null;
                if (!isBackdoor(token)){
                    assertAndRefreshToken(token, secureType);
                    jwtPayload = JwtHelper.getJwtPayload(token);
                }
                invokable.setModUser(jwtPayload != null ? jwtPayload.getPreferredUsername() : "null");
            }

            return invokable.invoke();
        } catch (BtbException bee) {
            logger.debug("Caught BackendException: " + bee.getClass().getSimpleName());
            return Response.status(bee.getHttpStatus()).entity(BtbExceptionMapper.toJson(bee))
                    .build();

        } catch (Exception e) {
            logger.error("Unexpected exception", e);
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
        }

    }

    private boolean isBackdoor(String token) {
        // backdoor is only available when the version(POM) contains "DEVELOP" or "SNAPSHOT"
        return DEVELOP_MODE && LET_ME_IN.equals(token);

    }
}
