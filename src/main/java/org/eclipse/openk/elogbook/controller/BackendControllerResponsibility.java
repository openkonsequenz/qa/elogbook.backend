/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;

import static org.eclipse.openk.elogbook.common.mapper.ResponsibilityMapper.PROCESS_MODE.CONFIRM;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.mapper.HResponsibilityMapper;
import org.eclipse.openk.elogbook.common.mapper.ResponsibilityMapper;
import org.eclipse.openk.elogbook.common.util.Comparators.TblResponsibilityIdComparator;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.exceptions.BtbGone;
import org.eclipse.openk.elogbook.exceptions.BtbInternalServerError;
import org.eclipse.openk.elogbook.persistence.dao.AutoCloseEntityManager;
import org.eclipse.openk.elogbook.persistence.dao.EntityHelper;
import org.eclipse.openk.elogbook.persistence.dao.HTblResponsibilityDao;
import org.eclipse.openk.elogbook.persistence.dao.RefBranchDao;
import org.eclipse.openk.elogbook.persistence.dao.RefGridTerritoryDao;
import org.eclipse.openk.elogbook.persistence.dao.TblResponsibilityDao;
import org.eclipse.openk.elogbook.persistence.model.HTblResponsibility;
import org.eclipse.openk.elogbook.persistence.model.TblResponsibility;
import org.eclipse.openk.elogbook.viewmodel.HistoricalShiftChanges;
import org.eclipse.openk.elogbook.viewmodel.ResponsibilitySearchFilter;
import org.eclipse.openk.elogbook.viewmodel.TerritoryResponsibility;

public class BackendControllerResponsibility {

	private static final Logger LOGGER = LogManager.getLogger(BackendControllerResponsibility.class.getName());
	public static final String MSG_DATA_OUTDATED_CATCHING_NEW_DATASET = "Data outdated catching new dataset";

	public enum POST_RESPONSIBILITY_MODE { PLAN, FETCH }

	private static ResponsibilityMapper responsibilityMapper = null;
	private static HResponsibilityMapper historicalResponsibilityMapper = null;

	public List<TerritoryResponsibility> getCurrentResponsibilities(String modUser) {
		LOGGER.debug("getCurrentResponsibilities() is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		List<TblResponsibility> tblResponsibilityList;
		List<TerritoryResponsibility> territoryResponsibilities;
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			TblResponsibilityDao dao = new TblResponsibilityDao(em);
			tblResponsibilityList = dao.getResponsibilitiesForUser(modUser);
			territoryResponsibilities = getResponsibilityMapper(em).mapToContainerVModelList(tblResponsibilityList);

		} finally {
			LOGGER.debug("getCurrentResponsibilities is finished");
		}

		LOGGER.debug("getCurrentResponsibilities() finished");
		return territoryResponsibilities;

	}

	public List<TerritoryResponsibility> getPlannedResponsibilities(String modUser) {
		LOGGER.debug("getPlannedResponsibilities() is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		List<TblResponsibility> tblResponsibilityList;
		List<TerritoryResponsibility> territoryResponsibilities;
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			TblResponsibilityDao dao = new TblResponsibilityDao(em);
			tblResponsibilityList = dao.getPlannedResponsibilitiesForUser(modUser);
			territoryResponsibilities = getResponsibilityMapper(em).mapToContainerVModelList(tblResponsibilityList);

		} finally {
			LOGGER.debug("getPlannedResponsibilities is finished");
		}

		LOGGER.debug("getPlannedResponsibilities() finished");
		return territoryResponsibilities;

	}

	public List<TerritoryResponsibility> getAllResponsibilities() {
		LOGGER.debug("getAllResponsibilities() is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		List<TblResponsibility> tblResponsibilityList;
		List<TerritoryResponsibility> territoryResponsibilities;
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			TblResponsibilityDao dao = new TblResponsibilityDao(em);
			tblResponsibilityList = dao.getAllResponsibilities();
			territoryResponsibilities = getResponsibilityMapper(em).mapToContainerVModelList(tblResponsibilityList);

		} finally {
			LOGGER.debug("getAllResponsibilities is finished");
		}

		LOGGER.debug("getAllResponsibilities() finished");
		return territoryResponsibilities;

	}

	public List<TerritoryResponsibility> getHistoricalResponsibilitiesByTransactionId(Integer transactionId) {
		LOGGER.debug("getHistoricalResponsibilitiesByTransactionId is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		List<HTblResponsibility> htblResponsibilityList;
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			HTblResponsibilityDao hdao = new HTblResponsibilityDao(em);
			htblResponsibilityList = hdao.getHistoricalResponsibilitiesByTransactionId(transactionId);
			return getHistoricalResponsibilityMapper(em).mapToContainerVModelList(htblResponsibilityList);
		} finally {
			LOGGER.debug("getHistoricalResponsibilitiesByTransactionId is finished");
		}
	}

	public List<TerritoryResponsibility> postResponsibilities(List<TerritoryResponsibility> territoryResponsibilityList,
															  String modUser, POST_RESPONSIBILITY_MODE mode ) throws BtbException {
		LOGGER.debug("postResponsibilities is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			em.getTransaction().begin();

			TblResponsibilityDao tblResponsibilityDao = new TblResponsibilityDao(em);
			List<TerritoryResponsibility> territoryResponsibilities;

			if( mode == POST_RESPONSIBILITY_MODE.PLAN ) {
				territoryResponsibilities = storePlannedResponsibilitiesInDB(
						territoryResponsibilityList, tblResponsibilityDao, getResponsibilityMapper(em), modUser);
			}
			else {
				territoryResponsibilities = storeFetchedResponsibilitiesInDB(
						territoryResponsibilityList, tblResponsibilityDao, getResponsibilityMapper(em), modUser);
			}
			em.getTransaction().commit();
			return territoryResponsibilities;

		} finally {
			LOGGER.debug("postResponsibilities is finished");
		}
	}

	public List<TerritoryResponsibility> confirmResponsibilities(
			List<TerritoryResponsibility> territoryResponsibilityList, String modUser) throws BtbException {
		LOGGER.debug("confirmResponsibilities is called");
		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			em.getTransaction().begin();

			TblResponsibilityDao tblResponsibilityDao = new TblResponsibilityDao(em);
			HTblResponsibilityDao htblResponsibilityDao = new HTblResponsibilityDao(em);
			List<TblResponsibility> plannedResponsibilitiesCurrentUser = tblResponsibilityDao
					.getPlannedResponsibilitiesForUser(modUser);

			List<TblResponsibility> originalPlannedResponsibilitiesCurrentUser = new ArrayList<>();
			for (TblResponsibility tblResponsibility : plannedResponsibilitiesCurrentUser) {
				originalPlannedResponsibilitiesCurrentUser
						.add((TblResponsibility) SerializationUtils.clone(tblResponsibility));
			}

			List<TerritoryResponsibility> territoryResponsibilities;
			try {
				territoryResponsibilities = storeConfirmedResponsibilitiesInDBAndHistorize(territoryResponsibilityList,
						tblResponsibilityDao, htblResponsibilityDao, getResponsibilityMapper(em), modUser,
						plannedResponsibilitiesCurrentUser, originalPlannedResponsibilitiesCurrentUser);
			} catch (BtbGone btbGone) {
				LOGGER.debug(MSG_DATA_OUTDATED_CATCHING_NEW_DATASET, btbGone);
				em.getTransaction().rollback();
				return getResponsibilityMapper(em).mapToContainerVModelList(originalPlannedResponsibilitiesCurrentUser);
			}

			em.getTransaction().commit();
			return territoryResponsibilities;

		} finally {
			LOGGER.debug("confirmResponsibilities is finished");
		}
	}

	/**
	 * Find all shift changes between start date and end date (given as parameters) and return them in a list.
	 *
	 * @param filter
	 *            the {@link ResponsibilitySearchFilter}
	 * @return a list containing all shift changes over the specified period
	 * @throws BtbException
	 *             if an error occurs
	 */
	public HistoricalShiftChanges getHistoricalShiftChanges(ResponsibilitySearchFilter filter) throws BtbException {
		LOGGER.debug("getHistgoricalShiftChanges(responsibilitySearchFilter) is called");
		LOGGER.debug("-> ResponsibilitySearchFilter=" + JsonGeneratorBase.getGson().toJson(filter));

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			HTblResponsibilityDao hTblResponsibilityDao = new HTblResponsibilityDao();
			Date transferDateFrom = filter.getTransferDateFrom();
			Date transferDateTo = filter.getTransferDateTo();
			List<HTblResponsibility> hTblResponsibilities = hTblResponsibilityDao
					.findHTblResponsibilitiesInPeriod(transferDateFrom, transferDateTo);
			return getHistoricalResponsibilityMapper(em).mapTblResponsibilitiesInPeriod(hTblResponsibilities,
					transferDateFrom, transferDateTo);
		} finally {
			LOGGER.debug("getHistoricalShiftChanges() is finished");
		}
	}

	public Boolean getIsObserver(String modUser) {
		LOGGER.debug("getIsObserver() is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		boolean isObserver = false;

		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			TblResponsibilityDao dao = new TblResponsibilityDao(em);
			List<TblResponsibility> tblResponsibilityList = dao.getNewOrCurrentResponisbleUser(modUser);
			if (tblResponsibilityList.isEmpty()) {
				isObserver = true;
			}

		} finally {
			LOGGER.debug("getIsObserver is finished");
		}

		LOGGER.debug("getIsObserver() finished");
		return isObserver;
	}

	private synchronized ResponsibilityMapper getResponsibilityMapper(EntityManager em) {
		if (responsibilityMapper == null) {
			LOGGER.debug("ResponsibilityMapper initializing");
			responsibilityMapper = new ResponsibilityMapper();
		}
		return responsibilityMapper;
	}

	private synchronized HResponsibilityMapper getHistoricalResponsibilityMapper(EntityManager em) {
		if (historicalResponsibilityMapper == null) {
			LOGGER.debug("HistoricalResponsibilityMapper initializing");
			historicalResponsibilityMapper = new HResponsibilityMapper(new RefGridTerritoryDao(em),
					new RefBranchDao(em));
		}
		return historicalResponsibilityMapper;
	}

	private static List<TerritoryResponsibility> storeFetchedResponsibilitiesInDB(
			List<TerritoryResponsibility> territoryResponsibilityList,
			TblResponsibilityDao tdao,
			ResponsibilityMapper responsibilityMapper,
			String modUser) throws BtbInternalServerError {

		if (territoryResponsibilityList != null && !territoryResponsibilityList.isEmpty()) {
			List<TblResponsibility> allResp = tdao.getAllResponsibilities();
			List<TblResponsibility> tblResponsibilityList;

			tblResponsibilityList = responsibilityMapper.mapFetchFromVModelList(territoryResponsibilityList,
						allResp, modUser);

			for (TblResponsibility tblResponsibility : tblResponsibilityList) {

				try {
					tdao.storeInTx(tblResponsibility);
				} catch (Exception e) {
					LOGGER.error("Error storing Responsibilities", e);
					throw new BtbInternalServerError("Error storing Responsibilities, e");
				}
			}
		}
		return null; // NOSONAR
	}

	private static List<TerritoryResponsibility> storePlannedResponsibilitiesInDB(
		List<TerritoryResponsibility> territoryResponsibilityList, TblResponsibilityDao tdao,
		ResponsibilityMapper responsibilityMapper, String modUser) throws BtbInternalServerError {

		if (territoryResponsibilityList != null && !territoryResponsibilityList.isEmpty()) {

			List<TblResponsibility> responsibilitiesForCurrentUser = tdao.getResponsibilitiesForUser(modUser);
			List<TblResponsibility> tblResponsibilityList;
			try {
				tblResponsibilityList = responsibilityMapper.mapFromVModelList(territoryResponsibilityList,
						responsibilitiesForCurrentUser, ResponsibilityMapper.PROCESS_MODE.PLAN, modUser);
			} catch (BtbGone btbGone) {
				LOGGER.debug(MSG_DATA_OUTDATED_CATCHING_NEW_DATASET, btbGone);
				return responsibilityMapper.mapToContainerVModelList(responsibilitiesForCurrentUser);
			}

			for (TblResponsibility tblResponsibility : tblResponsibilityList) {

				try {
					tdao.storeInTx(tblResponsibility);
				} catch (Exception e) {
					LOGGER.error("Error storing Responsibilities", e);
					throw new BtbInternalServerError("Error storing Responsibilities, e");
				}
			}
		}
		return null; // NOSONAR an empty list could also be returned as an actual result of a user for example with zero
						// responsibilities
		// but we have to differentiate between actual results and outdated dataset of responsibilities.
	}

	private static List<TerritoryResponsibility> storeConfirmedResponsibilitiesInDBAndHistorize(
			List<TerritoryResponsibility> territoryResponsibilityList, TblResponsibilityDao tdao,
			HTblResponsibilityDao htdao, ResponsibilityMapper responsibilityMapper, String modUser,
			List<TblResponsibility> plannedResponsibilitiesCurrentUser,
			List<TblResponsibility> originalPlannedResponsibilitiesCurrentUser) throws BtbInternalServerError, BtbGone {

		if (territoryResponsibilityList != null && !territoryResponsibilityList.isEmpty()) {

			List<TblResponsibility> tblResponsibilityList = responsibilityMapper.mapFromVModelList(
					territoryResponsibilityList, plannedResponsibilitiesCurrentUser, CONFIRM, modUser);

			List<HTblResponsibility> historyConfirmedResponsibilities = createHistoryConfirmedResponsibilities(
					tblResponsibilityList, originalPlannedResponsibilitiesCurrentUser, htdao, modUser);

			storeConfirmedResponsibilitiesInDBForHistory(historyConfirmedResponsibilities, htdao);
			storeConfirmedResponsibilitiesInDBinTx(tblResponsibilityList, tdao);

		}
		return null; // NOSONAR an empty list could also be returned as an actual result of a user for example with zero
						// responsibilities
		// but we have to differentiate between actual results and outdated dataset of responsibilities.
	}

	private static List<HTblResponsibility> createHistoryConfirmedResponsibilities(
			List<TblResponsibility> tblResponsibilityList,
			List<TblResponsibility> originalPlannedResponsibilitiesCurrentUser, HTblResponsibilityDao htdao,
			String modUser) throws BtbInternalServerError {
		LOGGER.debug("createHistoryConfirmedResponsibilities is called");

		Timestamp now = new Timestamp(System.currentTimeMillis());
		Integer lastTransactionId = htdao.getLastTransactionId();
		lastTransactionId++;

		originalPlannedResponsibilitiesCurrentUser.sort(new TblResponsibilityIdComparator());
		tblResponsibilityList.sort(new TblResponsibilityIdComparator());

		List<HTblResponsibility> hTblResponsibilityList = new ArrayList<>();
		for (int i = 0; i < tblResponsibilityList.size(); i++) {
			TblResponsibility tblResponsibility = tblResponsibilityList.get(i);
			if (!tblResponsibility.getResponsibleUser().equals(modUser)) {
				continue;
			}
			TblResponsibility originalPlannedResposibility = originalPlannedResponsibilitiesCurrentUser.get(i);
			HTblResponsibility hTblResponsibility = HResponsibilityMapper.mapFromTblResponsibility(tblResponsibility);
			hTblResponsibility.setFormerResponsibleUser(originalPlannedResposibility.getResponsibleUser());
			hTblResponsibility.setTransactionId(lastTransactionId);
			hTblResponsibility.setTransferDate(now);
			hTblResponsibilityList.add(hTblResponsibility);
		}

		LOGGER.debug("createHistoryConfirmedResponsibilities is finished");
		return hTblResponsibilityList;
	}

	private static void storeConfirmedResponsibilitiesInDBinTx(List<TblResponsibility> tblResponsibilityList,
			TblResponsibilityDao tdao) throws BtbInternalServerError {
		for (TblResponsibility tblResponsibility : tblResponsibilityList) {

			try {
				tdao.storeInTx(tblResponsibility);
			} catch (Exception e) {
				LOGGER.error("Error storing ConfirmationResponsibilities", e);
				throw new BtbInternalServerError("Error storing ConfirmationResponsibilities, e");
			}
		}
	}

	private static void storeConfirmedResponsibilitiesInDBForHistory(List<HTblResponsibility> htblResponsibilityList,
			HTblResponsibilityDao htdao) throws BtbInternalServerError {
		for (HTblResponsibility tblResponsibility : htblResponsibilityList) {

			try {
				htdao.storeInTx(tblResponsibility);
			} catch (Exception e) {
				LOGGER.error("Error storing History ConfirmationResponsibilities", e);
				throw new BtbInternalServerError("Error storing History ConfirmationResponsibilities, e");
			}
		}
	}
}
